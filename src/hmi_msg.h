#include <avr/pgmspace.h>
#ifndef HMI_MSG_H
#define HMI_MSG_H

/* Strings */
#define STUD_NAME "Kristiina Keelmann"
#define VER_FW "Version: " FW_VERSION " built on: " __DATE__ " " __TIME__ "\r\n"
#define VER_LIBC "avr-libc version: " __AVR_LIBC_VERSION_STRING__ " avr-gcc version: " __VERSION__ "\r\n"
#define VER_GCC "avr-gcc version: " __VERSION__"\r\n"
#define MEM_STAT_CMD "memstat"
#define MEM_STAT_HELP "Print memory usage and change compared to previous call"

extern char const stud_name[];
extern PGM_P const numbers[];

#endif
