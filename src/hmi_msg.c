#include <avr/pgmspace.h>
#include "hmi_msg.h"

char const stud_name[] PROGMEM = "Kristiina Keelmann";

const char a0[] PROGMEM = "Zero";
const char a1[] PROGMEM = "One";
const char a2[] PROGMEM = "Two";
const char a3[] PROGMEM = "Three";
const char a4[] PROGMEM = "Four";
const char a5[] PROGMEM = "Five";
const char a6[] PROGMEM = "Six";
const char a7[] PROGMEM = "Seven";
const char a8[] PROGMEM = "Eight";
const char a9[] PROGMEM = "Nine";

PGM_P const numbers[] PROGMEM = {a0, a1, a2, a3, a4, a5, a6, a7, a8, a9};
