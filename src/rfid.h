#ifndef RFID_H
#define RFID_H

void rfid_read (const char *const *argv);
void rfid_new (const char *const *argv);
void rfid_remove (const char *const *argv);
void rfid_show (void);
char *find_user (char *user_uid);

#endif
